function RunFeederAndPlayTone()
%RunFeederAndPlayTone Runs the feeder and plays a reward tone for in-cage training
%   Runs the HDE fish feeder and the speaker for in-cage traininging. Must be run after the
%   ESP8266 chip is plugged in and set up. If the
%   code is not working, check to see that the computer is running on
%   bioe-i3xx-local Wi-Fi.
%
%   Devon Griggs and Bill Conley
%   7/23/2019

system(['curl --silent http://',getIP,'/MotorAndTone > /dev/null']);

% % Below is an alternate example approach to interfacing wirelessly. The execution may
% % be a bit faster but it opens up a new window each time. 
% [stat,browser]=web('192.168.1.111/Motor');
% pause(1);
% close(browser);
% if stat ~= 0
%     disp('MATLAB could not reach the IP location.')
% end
end

