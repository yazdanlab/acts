# ACTS: Automated Cage-side Training System

This code is meant to run ACTS based on the forthcoming publication by Griggs et al. from the lab of Azadeh Yazdan-Shahmorad, University of Washington. This repo also contains the data collected and used for figures. 

## Getting Started

Copy the ACTS repo to your Mac. 
Download MATLAB
Download Arduino IDE
Download Psychtoolbox 3 and safe into the ACTS directory. http://psychtoolbox.org/
Download Yam Display to your Mac and the associated app to your iPad. http://psychtoolbox.org/

Open Arduino IDE and configure to write code to the ESP8266 via the USB to ESP-01 Adapter. The Adapter comes in different versions which may require different drivers. Ref. https://tttapa.github.io/ESP8266/Chap03%20-%20Software.html
Edit WiFi_Test.ino to include your local private WiFi network and password. Load WiFi_Test.ino onto the ESP8266 via the USB to ESP-01 Adapter. Note the switch must be set to PROG when loading code, and otherwise when running code. Note that a powered USB port may be required to load and run the code. 

Wire the ESP8266. Turn off the speaker. Power on the ESP8266 and wait for flashes indicating a connection has been established. Edit getIP.m by following the comments to hardcode the correct IP address. 

Connect both your Mac and your iPad to the same local private Wi-Fi network.
Use Yam to setup your iPad as a second monitor on the right side of your Mac.
Make the MATLAB filepath include ACTS and all folders and subfolders.
Use MATLAB to open runExperimentiPad.m and Parameters_CONSTANT.m and edit the lines called out by "%%%UPDATE:" to match your Mac and iPad.

Use MATLAB to run runExperimentiPad.m after the electronics are wired appropriately. 
Play the game on the iPad. 
End the game with Ctrl+C in the MATLAB command window.
View the data collected in MATLAB.

## Physical Build

The system was designed with flexibility in mind for various cage designs. The steps below describe the assembly for the rig to fit Seattle-type cages. 

## Components list

The components listed below can all be purchased from 80/20.net. We have listed the part numbers and the quantity required of each:

* Part no. 1010: 120 inches
* Part no. 3386: 38 pcs
* Part no. 4108: 21 pcs
* Part no. 2015: 5 pcs
* Part no. 6852: 8 pcs
* Part no. 3923: 12 pcs
* Part no. 3066: 8 pcs
* Part no. 3663: 10 pcs

The following components were sourced from various locations. Specifics for those components have been included below:

* Laptop (MacBook Air, Early 2015, 1.6 GHz Intel Core i5, 8 GB 1600 MHz DDR3, Intel HD Graphics 6000 1536 MB, OS X Yosemite version 10.10.5; Apple Inc.)
* Tablet (iPad, MD510LL/A, version 10.3.3 (14G60); Apple Inc.)
* WIFI router (WRT54GL, Wireless-G 2.4 GHz, 54 Mbps; Linksys)
* Automatic Fish Feeder (HDE)
* Speaker (Adafruit.com, Product ID: 1314)
* Zinc plated corner brace (Home Depot)
* Wiring
* Solder
* Super Glue
* ESP-01S and USB adapter
* Lab Tape
* Cable Ties
* Electrical Switch
* Funnel
* Tubing 
* Electrical Box
* Two small padlocks

The required software is listed below as well as what component it pairs with:

* YAM Display (Tablet)
* Psychophysics Toolbox 3 (Laptop)
* CH240G driver (for ESP-01 to USB Adapter) (Laptop)
* MATLAB (Laptop)
* Arduino IDE (Laptop)

## Frame Construction

The frame of the unit is made out of 1" x 1" T-Slotted Profile (part no. 1010). From the length ordered, you need 4 pieces at 31 cm, 4 pieces at 25 , and 4 pieces at 20 cm. The 4 pieces measuring 25 cm will make up the height of the structure and the other pieces will anchor to those. 

## Rear and Side Faces

Take 2 of the 25 cm pieces and 2 of the 31 cm pieces as well as 4 of the Inside corner brackets and 8 of the part no. 3386 bolt assemblies. 

 * Lay out the 4 bar pieces so that the 25 cm pieces are oriented vertically and the 31 cm pieces are oriented horizontally. 
 * In the faces of the bars that are facing each other, slide two of the t-nuts.
 * Line up the 4 bars so that they are making contact. The ends of the horizontal pieces should be touching the inside of the vertical pieces and they should be aligned flush to the top of the vertical pieces. 
 * Place the inside corner brackets in each corner and line up the t-nuts with the holes of the corner brackets. Thread in the bolts through the brackets into the t-nuts. 
 * This rectangle is the rear face of the frame. Use these same instructions to construct the two sides using the other two 25 cm bars for the vertical supports, and the 4 cm bars for the horizontal supports. 

## Front Face and iPad attachment 

The front of the structure also contains pieces and adjustments to support the iPad tablet required for the training modules and the bezel used to protect the iPad which will be attached at this time.

 * Orient the structure so the two vertical 25 cm pieces that do not have horizontal supports are facing you. 
 * Take one of the two remaining 31 cm bars and slide two of the t-nuts into one of the faces. This bar will slide in between the two vertical bars on the bottom with the side that has the t-nuts facing up and will attach to the vertical pieces using the inside corner pieces, bolt/t-nut assemblies. 
 * Take the iPad and rest the bezel horizontally in the lower support bar attached in the previous step with the screen facing forward. Lower the remaining 31 cm bar into place above the iPad so that the bezel of the tablet is inserted into the channels of the bar and attach to side supports using inner corner brackets and bolt/t-nut assemblies. Note: the top horizontal bar WILL NOT be flush with the top of the vertical supports because it is supporting the iPad. 

## Plastic Bezel Attachment

The plastic bezel that is protecting the tablet was cut out of a piece of scrap plastic. It was cut to 36 cm x 22.9 cm. In the middle of the plastic we also cut a rectangular window for the screen of the iPad to be visible. That window for the iPad we used was cut to 19.7 cm x 15.24 cm and was centered in the piece of plastic. Two holes on each of the four sides were also drilled for the screws to attach the bezel to the frame. The holes can be spaced anywhere along the four sides, they just need to be a half an inch in from the edge of the bezel to align with the channels on the bars. The 8 fasteners we used were part nos. 3663 (screws) and 3923 (nuts). 

 * With the front face of the frame facing up, slide two of the nuts into each of the vertical supports and each of the horizontal supports (you will need to remove the horizontal supports to slide the nuts in. Reattach them when complete).
 * Align the nuts with the holes in the plastic bezel and using the screws attach bezel to frame. 

## Cage Attachment Hooks

To attach the reward system to the primate cage, we used four zinc plated corner braces. The holes of these corner braces were drilled out to fit part no 3386 screws as well as small padlocks. It is also recommended to have the corners rounded so that the sharp corners can not injure the primates. 

* With the front of the frame facing you, loosen the bottom horizontal support on the front enough to be able to slide in two of the 3386 nuts on the bottom face and then put the bar back into place. 
* Using two of the corner braces and two of the part no 3386 screws, screw the braces to the frame upside down so that the side not attached to the frame can be placed through the cage bars, allowing the frame to rest on the cage. These can also be adjusted side to side to fit various size cages.
* Slide two of the part no 3386 nuts in the top of the two front vertical bars in the channels facing inward. Using two of the screws attach the remaining two corner braces so that the side of the brace not attached to the frame is resting against the cage bars. This allows for padlocks to be easily attached around cage bars and through the brace holes. 

## Electronics Box Attachment

The box that contains the feeder, speaker and other electronics is situated in the rear right corner of the frame. It is made of 4 pieces of scrap plastic. The bottom piece measured 21.3 cm long x 16.5 cm wide. Two side pieces were cut out that measured 6 16.5 cm x 9.2 cm. And the back piece of the box measured 21.3 cm x 9.2 cm. 80/20 pieces used were part nos. 3663 screw, 6852 wing nuts, 4108 inside corner brackets, 3923 roll in t-nuts. 

 * With the rear of the frame facing you, remove the top horizontal bars on the rear and right side of the frame. Slide one of the roll in t-nuts into each of the top channels of those horizontal bars and reattach them to the frame. 
 * The bottom piece of plastic was situated on the structure so that the middle of our electronics box was centered in that right rear corner of the frame. Mark a spot to drill a hole 12.5 mm from the edge of the plastic that lines up with the channels of the horizontal bars. Drill two holes at those marks. 
 * Line up the two roll in t-nuts with the holes you drilled in the plastic base, place an inside corner bracket above hole and screw the base to the frame. 
 * Using one of the two side pieces, mark and drill a hole on one of the longer sides to attach it to the already installed inside corner bracket on the base piece of the box. Thread a screw through the hole and use a wing nut to secure the side to the frame. Follow this same process for the back piece of the box.
 * For the remaining side piece, follow the same instructions, but use a wing nut to attach the side pieces to the base. 
 * For extra strength, drill a hole on the side and back pieces 12.5 mm in where the pieces meet making sure the holes line up. Use the inside corner bracket pieces, screw and wing nuts to attach them together. 

### Prompts to start the game

After running the runExperimentiPad.m script, a series of command line prompts will appear in order to correctly set up the desired game. The prompts and possible answers are listed below:

* Prompt: Name? 
 * Answer: type “test” for user testing, otherwise type name of the monkey you would like to save data for

* Prompt: Choose your task: (task options are then listed) 
 * Answer: type the number corresponding to the desired task (e.g. “1” for Instructed Delay Task)

* Prompt: Choosing Parameter File, Enter name or default
 * Answer: current options include “default” ( press enter key) or “training” (“training” only used when running the Training Task)

* Prompt: Run setup procedure? 
 * Answer: “y” or “n.” Choosing “y” will run through 5 non-recorded trials of the selected task allowing the user to ensure everything is working correctly, “n” will skip this section and proceed to the next step in the setup process. The prompt will reappear if “y” was initially chosen, requiring the user to choose “y” or “n” again. 

* Prompt: When the “Pause button is pressed, the game will pause after the current trial is complete. 
 * Answer: press enter key to continue

A beep should sound and the first trial will begin.

### Details about each task

There are currently three different tasks. Additional tasks can be easily added (further details below). The Instructed Delay Task (Task 1) requires the subject to hold a position in a starting circle, then after an audial cue the subject must move to a target circle for a specified hold time. The Two-Forced Choice Task (Task 2) is similar to the Instructed Delay task, the only difference being that there are two available target circles that can be chosen depending on two different audio cues. The Training Task (Task 3) displays green circles in random locations, requiring the subject to touch and hold on the circle for a specified amount of time. 

All tasks are separate functions and .m files. A new task can be added by creating a new function and file for the task. In runExperimentiPad.m under the subsection “Choose Task,” add the new task name under the switch case, “taskChosen.” This will allow the game results to be tracked on a GUI and saved (Figure 1). Add the new task under the same switch case label in the “Start of Experiment” subsection as well (Figure 2). Call the function for the new task in this section. The function will be called for each different trial of the game. If other variables are needed to run the function, add them in this location as needed (e.g. Two-Forced Choice Task requires adaptive probability variables to be set up and calculated before the function runs for each trial). In this same section in the script below there is another switch case where the new task must be added so the GUI stats are updated (Figure 3).

To avoid issues with adding a new task, go to all instances of the switch case for “taskChosen” to ensure all needed information for the new task is added and updated correctly.

![figure_1](pics/pic1.png)
*Figure 1. In the “%% Choose Task” section under the “taskChosen” switch case, all available task options are listed in order for results to be tracked on the GUI. This section sets up the GUI.*

![figure_2](pics/pic2.png)
*Figure 2. The “%% Start of Experiment” section calls the function for the pre-selected task under the “taskChosen” switch case. Each iteration of the while loop goes through one trial.*

![figure_3](pics/pic3.png)
*Figure 3. Under the “%% Start of Experiment” section, there is a switch case for “taskChosen” in order to update the values on the GUI in real time.*

### Parameter files

There is a Parameters_CONSTANT, and a Parameters_EDIT script. Parameters_EDIT contains parameters that may need to be changed from trial to trial including circle size or hold time. Parameters_CONSTANT contains more permanent parameters such as game window position and cursor size.

There are currently two parameter file choices for Parameters_EDIT: “default” and “training.” The default file can be used for Task 1 and 2, and the training parameter file should be used for Task 3 (Training Task). The “training” parameter option contains various additional variables specific to the training task not contained in “default.”

New parameter file options can be added by adding a new label under the switch case “parameterSet” in the Parameter_EDIT.m file (Figure 4). All the parameter variables present in “default” must still be present in the new parameter case, but additional variables that are specific to the new parameter file can be added. 

New parameter files may be added for additional tasks, or to allow for parameter files to be specific to different test subjects. For example, subject A may be learning a task more quickly than subject B, and in this case a custom parameter file can be created for each subject. This can allow parameter values to be varied for each test subject independently and ease the burden on the researcher when switching between animals that require different parameter values.

![figure_4](pics/pic4.png)
*Figure 4. Parameters_EDIT file, switch cases shown under the label “parameterSet.” New parameter files can be added here.*

### Saved Data 

Under the first subsection of the runExperimentiPad.m file there are two switch cases: computerType and macType (Figure 5). Within these cases a directory is created for the saved data and there exists the file path to which the collected data is saved. The file path variable name is “path_saveData.” The collected data is then saved at the end of each trial (Figure 6), the data structure from the previous trial being overridden to avoid saving any repeat data. This method ensures that in the event of the game ending unintentionally, the data will not be lost. The saved .mat files are stored on the user’s computer under the file path stated in the script (Figure 5).

![figure_5](pics/pic5.png)
*Figure 5. Toward the top of the script the save directory is created and the save file path for the data is stored under variable name “path_saveData.”*

![figure_6](pics/pic6.png)
*Figure 6. At the end of each trial the cumulative new data is saved and overrides the old data from from the previous trial.*

### Prioritizing Computer Tasks

In order to have the system run with the most efficiency, it is beneficial to manually prioritize the tasks the computer is running. The highest priority task should be Yam Display, followed by MATLAB. In order to manually set priorities, follow the steps below (for an iOS system):

* Open the Activity Monitor application (figure 7)

* Read the “PID” (process ID) for the task to prioritize (right most column in figure 7)

* Open terminal (figure 8)

* Type in the command line as seen below: ps -fl -C <insert PID>, then press enter

* The number printed directly under “NI” is the number determining task priority that can be changed. -20 represents the highest priority task while 20 represents the lowest priority task (in figure 8 the “NI” number is 0).

* To change the priority number type the following in the command line: sudo renice -n <insert number to add to current NI number> -p <insert PID number>

* To ensure this worked, repeat step 4 to see the new “NI” number

![figure_7](pics/pic7.png)
*Figure 7: Activity monitor with PID number on the right side.*

![figure_8](pics/pic8.png)
*Figure 8: Terminal window with commands listed above.*

End of file.
