function RunFeeder()
%RunFeeder Runs the feeder for in-cage training
%   Runs the HDE fish feeder for in-cage traininging. Must be run after the
%   ESP8266 chip is plugged in and set up. This code pings what is usually
%   the IP address of the chip to cause the motor to start spinning. If the
%   code is not working, check to see that the computer is running on
%   bioe-i3xx-local Wi-Fi.
%
%   Devon Griggs and Bill Conley
%   7/23/2019

system(['curl --silent http://',getIP,'/Motor > /dev/null']);

% % Below is an alternate approach to running the feeder. The execution may
% % be a bit faster but it opens up a new window each time. 
% [stat,browser]=web('192.168.1.111/Motor');
% pause(1);
% close(browser);
% if stat ~= 0
%     disp('MATLAB could not reach the IP location.')
% end
end

