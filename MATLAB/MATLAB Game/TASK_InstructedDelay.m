% Shivalika Chavan, Kali Coubrough
% May 6, 2019
% Yazdan Lab
% Instructed Delay Task - task where a target circle appears after a tone
% (time is randomized every trial), must hold in initial and target circles
% for a set amount of time

function [angle, X, Y, timeStamp, successful, completionTime, extraTimeOut, data_3d, counts] = TASK_InstructedDelay(usingMotive, TASK, SETUP, natnetclient, window, computerType, macType, PCname, humanWindow)

% build initial circle
center_X = 0.5*SETUP.xWidth;
center_Y = 0.5*SETUP.yWidth;
radius = TASK.startCircleSize;
x1 = center_X-radius;
y1 = SETUP.yWidth-(3*radius);
x2 = center_X+radius;
y2 = SETUP.yWidth - radius;

%build target circle
[addInXDir, addInYDir, angle] = buildTargetCircle(x1, x2, y1, y2, TASK, SETUP);

% initialize arrays for screen x/y coordinates, computer time stamps, and
% data structure
maxIndices = TASK.reachDistance*60;
Y = zeros(1, maxIndices);
X = zeros(1, maxIndices);
timeStamp = nan(1, maxIndices);
data_3d = struct('names', [], 'x_3d', [], 'y_3d', [], 'z_3d', []);

% reset variables that are used every trial
successful = true; 
inTarget = false; %checks in each iteration if the cursor is in the end target
showTarget = false; % is false until inStart reaches the desired number of counts
showStartCircle = false;
doneWithTask = false;
beeped = false;

% determines if hold will be taken into account
holdTouch = TASK.holdTouch;

% reset timers
startTimer_Start = -1;
startTimer_Target = -1;
time_showTarget = -1;
bufferStartTime = -1;

% randomize the go tone - max time is indicated by waitTime_BeforeGoTone,
% can be edited in Parameters_EDIT.m
waitSecs_BeforeGoTone = rand(1)*TASK.waitTime_BeforeGoTone; 

ii = 0; % loop variable - increments every time the screen flips (is updated)
startTask = GetSecs;
touchedStart = false;
touchedTarget = false;
extraTimeOut = 0;
buffer = false;

while (~doneWithTask || buffer)
    ii = ii+1;
    timeStamp(ii) = GetSecs-startTask; 
    if buffer
        Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
        Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
        if timeStamp(ii)-bufferStartTime > TASK.bufferTime
            break
        end
    end        
  
    [X, Y, data_3d] = getCursor(usingMotive, natnetclient, SETUP, X, Y, data_3d, ii, computerType, macType, PCname);
    x = X(ii);
    switch computerType
        case'MACI64'
            x = x - SETUP.xMin;
    end
    y = Y(ii);
    
    switch computerType
        case 'PCWIN64'
            Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);
        case'MACI64'
            Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);
            Screen('DrawDots', humanWindow, [x y]./2, (SETUP.cursorSize)./2, [255 255 255], [], 2);
    end
    Screen('Flip',window);
    Screen('Flip',humanWindow);
    
    if buffer
        continue
    end
    
    % after one second after the start of the task, show the initial circle
    if(GetSecs-startTask>TASK.TimeToShowCircle)
        Screen('FillOval',window, TASK.startTargetColor,[x1 y1 x2 y2]);
        Screen('FillOval',humanWindow, TASK.startTargetColor,[x1 y1 x2 y2]./2);
        showStartCircle = true;
    end
        
    if(pdist([x y; mean([x1 x2]) mean([y1 y2])],'euclidean') <= radius && showStartCircle) %if cursor is within initial circle
        inStart = true;
        touchedStart = true;
        
    else % if not in the circle, then not inStart and the stopwatch for staying in start target resets
        inStart = false;
    end
            
    % the stopwatches for the start and target timers should only start
    % when both the cursor is within the desired circle and the respective 
    % timer has not started
    
    switch computerType
        case 'PCWIN64'
            
            if (inStart && startTimer_Start == -1)
                startTimer_Start = GetSecs;
            end
            
            if(inTarget && startTimer_Target == -1)
                startTimer_Target = GetSecs;
            end
            
        case 'MACI64'
            
            if (inStart && startTimer_Start == -1)
                switch holdTouch
                    case 'yes'
                        [~,~,button] = GetMouse(); % Check to see if the mou3se is held down for timer to start
                        if(button(1) == 1)
                            startTimer_Start = GetSecs;
                        end
                    case 'no'
                        startTimer_Start = GetSecs;
                end
            end
            
            if(inTarget && startTimer_Target == -1)
                switch holdTouch
                    case 'yes'
                        [~,~,button] = GetMouse(); % Check to see if the mouse is held down for timer to start
                        if(button(1) == 1)
                            startTimer_Target = GetSecs;
                        end
                    case 'no'
                        startTimer_Target = GetSecs;
                end
            end
            
    end
    
    % if the start timer has been started (equal to anything other than -1)
    if(~(startTimer_Start == -1))
        % if the the initial circle timer shows a time greater than what is needed to show the target
        % AND the target has not been shown yet, then show the target -
        % This should only happen once per trial (any other outcomes are a
        % penalty)
        if(GetSecs-startTimer_Start > TASK.holdInStart && ~showTarget)
            showTarget = true;
        end
    end
    
    % reference if statement above - same logic but with the target circle
    if(~(startTimer_Target == -1))
        if(GetSecs-startTimer_Target > TASK.holdInEnd)
            successful = true;
            doneWithTask = true;
        end
    end

    % if the target should be shown (either true or false)
    if(showTarget)
        Screen('FillOval',window,TASK.endTargetColor,[x1+addInXDir y1-addInYDir x2+addInXDir y2-addInYDir])
        Screen('FillOval',humanWindow,TASK.endTargetColor,[x1+addInXDir y1-addInYDir x2+addInXDir y2-addInYDir]./2)        
        
        %There is another timer that starts when the target circle appears.
        % This starts that time if it has not started yet.
        if(time_showTarget == -1)
            time_showTarget = GetSecs;
        end
        
        % if the tone has not sounded and the timer has surpassed what is
        % necessary for the beep, then beep -- successful trial so far
        if(~beeped && GetSecs-time_showTarget>waitSecs_BeforeGoTone)
            switch computerType
                case 'MACI64'
                    PlayTone(0);
                    Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);

                case 'PCWIN64'
                    Beeper(TASK.go_frq, TASK.volume, TASK.go_dur);
            end
            beeped = true;
        end
    end
     
    %check if in target circle
    if(pdist([x y; mean([x1 x2])+addInXDir mean([y1 y2])-addInYDir],'euclidean') <= radius && beeped)
        inTarget = true;
        touchedStart = false;
        touchedTarget = true;
    else    
        inTarget = false;
    end
    
    % penalty for not holding in a circle
    if (~beeped && ~inStart && touchedStart) || (beeped && ~inTarget && touchedTarget)
            doneWithTask=true;
            extraTimeOut = TASK.TIMEOUT_Hold;
            successful = false;
            if(touchedStart)
                SETUP.holdInStartPenalties = SETUP.holdInStartPenalties+1;
            elseif(touchedTarget)
                SETUP.holdInTargetPenalties = SETUP.holdInTargetPenalties+1;
            end
            SETUP.fails = SETUP.fails+1;
            buffer = true;
    end
    
    %penalty for taking too long
    if(GetSecs-startTask > TASK.maxTime)
        extraTimeOut = TASK.TIMEOUT_MaxTime;
        successful = false;
        doneWithTask = true;
        SETUP.maxTimePenalties = SETUP.maxTimePenalties+1;
        SETUP.fails = SETUP.fails+1;
        buffer = true;
    end
    
    % The success tone plays before the circle disappears
    if(successful && doneWithTask)
        filename = 'success_tone.wav';
        [n, Fs] = audioread(filename);
        sound(n(26000:150000),Fs);
        switch computerType
            case 'MACI64'
                RunFeederAndPlayTone;
            case 'PCWIN64'
                % insert reward command or do nothing
        end
        buffer = true; % added 11/8
    end
    
    % finish with a black screen when done with task
    if(doneWithTask)
        buffer = true;
        Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
        Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
        Screen('Flip',window);
        Screen('Flip',humanWindow);

    end
    
    if buffer
        bufferStartTime = timeStamp(ii);
    end
end

X = X';
Y = Y';


completionTime = GetSecs-startTask;

% Updates the number of successes after each trial
if(successful)
    SETUP.successes = SETUP.successes+1;
end
counts = [SETUP.successes, SETUP.holdInStartPenalties, SETUP.maxTimePenalties, SETUP.holdInTargetPenalties];
end