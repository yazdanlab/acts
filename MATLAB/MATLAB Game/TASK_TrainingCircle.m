% Shivalika Chavan, Kali Coubrough test 
% May 6, 2019
% Yazdan Lab
% simple training circle task - touch the circle that appears on the
% screen (location is randomized, size is predetermined), possible to have
% a hidden radius within the visible circle

function [X, Y, timeStamp, successful, completionTime, extraTimeOut, data_3d, counts] = TASK_TrainingCircle(usingMotive, TASK, SETUP, natnetclient, window, computerType, macType, PCname, humanWindow)
% build circle
setTarget = false;
radius = TASK.CircleSize;
hiddenRadius = TASK.hiddenRadius; % invisible radius 

maxDimension = max([SETUP.xWidth SETUP.yWidth]);

% determines if hold will be taken into account
holdTouch = TASK.holdTouch;

% fill up entire screen with target
if TASK.CircleSize >= maxDimension/2
    center_X = 0.5*SETUP.xWidth; 
    center_Y = 0.5*SETUP.yWidth;
    x1 = center_X-radius; 
    y1 = center_Y-radius; 
    x2 = center_X+radius; 
    y2 = center_Y+radius;
    setTarget = true;
end

% randomize location of target
while ~setTarget
    center_X = rand*SETUP.xWidth; 
    center_Y = rand*SETUP.yWidth;
    x1 = center_X-radius; 
    y1 = center_Y-radius; 
    x2 = center_X+radius; 
    y2 = center_Y+radius;
    if(x1>=0 && y1>=0 && x2<=SETUP.xWidth && y2<=SETUP.yWidth)
        setTarget = true;
    else
        setTarget = false;
    end
end


Y = zeros(1, 1300);
X = zeros(1, 1300);
timeStamp = nan(1, 1300);
successful = true;

inStart = false;
doneWithTask = false;

startTimer_Start = -1;
bufferStartTime = -1;

%first get coordinates, then flip screen for first iteration
%in each iteration after check where the cursor is and continue if
%necessary
ii = 0;
startTask = GetSecs;
touchedStart = false;
if ismac
    initialMouseLocation=[1550,1000];
    initialMouseLocation=[560,660];
    SetMouse(initialMouseLocation(1), initialMouseLocation(2));
end
data_3d = struct('names', [], 'x_3d', [], 'y_3d', [], 'z_3d', []);
extraTimeOut = 0;
buffer = false;
while (~doneWithTask || buffer)
    ii = ii + 1;
    timeStamp(ii) = GetSecs-startTask; %time stamp for this screen flip and set of xy coordinates
    %get and show position of cursor
    if buffer
        Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
        Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
        if timeStamp(ii)-bufferStartTime > TASK.bufferTime 
            break
        end
    end

    [X, Y, data_3d] = getCursor(usingMotive, natnetclient, SETUP, X, Y, data_3d, ii, computerType, macType, PCname);
    x = X(ii);
    switch computerType
        case'MACI64'
            x = x - SETUP.xMin;
    end
    y = Y(ii);
    
    switch computerType
        case 'PCWIN64'
            Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);
        case 'MACI64'
            Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);
            Screen('DrawDots', humanWindow, [x y]./2, (SETUP.cursorSize)./2, [255 255 255], [], 2);
    end
    
    Screen('Flip',window);
    Screen('Flip',humanWindow);
    
    if buffer
        Screen('Flip',window);
        continue;
    end
    
    % after x seconds after the start of the task, show the initial circle
    if(GetSecs-startTask>TASK.TimeToShowCircle)
        Screen('FillOval',window, TASK.startTargetColor,[x1 y1 x2 y2]);
        Screen('FillOval',humanWindow, TASK.startTargetColor,[x1 y1 x2 y2]./2);
    end
    
    if(pdist([x y; mean([x1 x2]) mean([y1 y2])],'euclidean') <= hiddenRadius) %if cursor is within initial circle
        inStart = true;
        touchedStart = true;
        switch holdTouch
            case 'yes'
                %nothing additional
            case 'no'
                successful=false;
                doneWithTask = true;
                Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
                Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
                Screen('Flip',window);
                Screen('Flip',humanWindow);    
        end
    else % if not in the circle, then not inStart and the stopwatch for staying in start target resets
        inStart = false;
        startTimer_Start = -1;
        switch computerType
            case 'PCWIN64'
            case'MACI64'
                % If the cursor has been moved but is not in the circle,
                % fail the trial. 
                if y~=initialMouseLocation(2) || x>=0
                    successful=false;
                    doneWithTask = true;
                    Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
                    Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
                    Screen('Flip',window);
                    Screen('Flip',humanWindow);
                    SETUP.holdInStartPenalties = SETUP.holdInStartPenalties+1;
                end
        end
    end
    
    % start timer
    switch computerType
        case 'PCWIN64'
            
            if (inStart && startTimer_Start == -1)
                startTimer_Start = GetSecs;
            end
            
        case 'MACI64'
            
            if (inStart && startTimer_Start == -1)
                [~,~,button] = GetMouse(); % Check to see if the mouse is held down for timer to start
                %if(button(1) == 1) % if any(button) try this!!
                if any(button)    
                    startTimer_Start = GetSecs;
                end
            end
            
    end
    
    % if the start timer has been started (equal to anything other than -1)
    if(~(startTimer_Start == -1))
        % if the the initial circle timer shows a time greater than what is needed to show the target 
        % AND the target has not been shown yet, then show the target - 
        % This should only happen once per trial (any other outcomes are a
        % penalty)
        if(GetSecs-startTimer_Start > TASK.holdInStart)
            disp('hold taken into account')
            doneWithTask = true;
            successful = true;
            buffer = true;
            % Clear the screen twice (solved a curious bug that occured
            % when the screen was only cleared once).
            Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
            Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
            Screen('Flip',window);
            Screen('Flip',humanWindow);
            Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
            Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
            Screen('Flip',window);
            Screen('Flip',humanWindow);
        end
    end

    if ~inStart && touchedStart
        doneWithTask=true;
        extraTimeOut = TASK.TIMEOUT_Hold;
        successful = false;
        SETUP.holdInStartPenalties = SETUP.holdInStartPenalties+1;
        buffer = true;
    end
    
    %penalty for taking too long
    if(GetSecs-startTask > TASK.maxTime)
        extraTimeOut = TASK.TIMEOUT_MaxTime;
        successful = false;
        doneWithTask = true;
        SETUP.maxTimePenalties = SETUP.maxTimePenalties+1;
        SETUP.fails = SETUP.fails+1;
        buffer = true;
    end
    
    
    % finish with a black screen when done with task
    if(doneWithTask)
        buffer = true;
        Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
        Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
    end
    
    
    % The success tone plays after the circle disappears
    if(inStart && doneWithTask)
        filename = 'success_tone.wav';
        [n, Fs] = audioread(filename);
        audiowrite(filename,n,Fs);
        Beeper;
        sound(n(26000:150000),Fs);
        switch computerType
            case 'MACI64'
                successful = true;
                RunFeederAndPlayTone;
            case 'PCWIN64'
                % insert reward command or do nothing
        end
    end
    
    if buffer
        bufferStartTime = timeStamp(ii);
    end
end

completionTime = GetSecs-startTask;

% Updates the number of successes after each trial
if(successful)
    SETUP.successes = SETUP.successes+1;
end
counts = [SETUP.successes, SETUP.maxTimePenalties, SETUP.holdInStartPenalties];

switch computerType
    case 'MACI64'
        SetMouse(initialMouseLocation(1), initialMouseLocation(2));
end
end

