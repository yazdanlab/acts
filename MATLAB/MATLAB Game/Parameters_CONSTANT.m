function setup = Parameters_CONSTANT(computerType, macType, PCname)

setup.numTrials = 10000;
setup.filename = strrep(['Trials_' strrep(datestr(now, 15), ' ', '_') '.mat'], ':', '-');

%% COORDINATE TRANSFORMATION 
setup.collectionTime = 5;
setup.collectionFRQ = 500;
setup.Xmultiplier = 3.35;
setup.Ymultiplier = 3.31;
setup.offset = 0.0;
            
%% SCREEN
switch computerType
    case 'MACI64'
        switch macType
            case 'IMAC'
                % position for game window
                setup.xMin = 2560; 
                setup.yMin = 0; 
                setup.xMax = 3360; 
                setup.yMax = 600;
                setup.xWidth = setup.xMax - setup.xMin; %800l
                setup.yWidth = setup.yMax - setup.yMin; %600
                % position for user window
                setup.Left = 1559; 
                setup.Bottom = 1000; 
                setup.Width = 1000; 
                setup.Height = 1000;
                setup.pausePosition = [50 870 200 100];
                setup.axisPosition = [50 100 500 500];
                setup.graphPosition = [50 600 500 50];
                setup.successRatePosition = [600 500 300 100];
                setup.recentSuccessRatePosition = [600 250 300 100];
                setup.correctTargetPosition = [600 100 300 100];
            case 'MACBOOK'
                % position for game window
                
                %%%UPDATE: BASED ON YOUR MAC AND IPAD SCREEN RESOLUTIONS
                setup.xMin = 1366; 
                setup.yMin = 0; 
                setup.xMax = 2390; 
                setup.yMax = 768;
                setup.xMinHuman = 0; 
                setup.yMinHuman = 0; 
                setup.xMaxHuman = 1366/2; 
                setup.yMaxHuman = 768/2;
                %%%
                
                setup.xWidth = setup.xMax - setup.xMin; %1024
                setup.yWidth = setup.yMax - setup.yMin; %768
                % position for user window
                setup.Left = 800; 
                setup.Bottom = 500; 
                setup.Width = 500; 
                setup.Height = 500;
                setup.pausePosition = [50 425 100 50];
                setup.axisPosition = [25 50 250 250];
                setup.graphPosition = [25 300 250 25];
                setup.successRatePosition = [300 250 150 50];
                setup.recentSuccessRatePosition = [300 125 150 50];
                setup.correctTargetPosition = [300 75 150 50];
            case 'PERSONAL LAPTOP'
                % position for game window
                setup.xMin = 0; 
                setup.yMin = 0; 
                setup.xMax = 500; 
                setup.yMax = 500;
                setup.xWidth = setup.xMax - setup.xMin; %100
                setup.yWidth = setup.yMax - setup.yMin; %100
                % position for user window
                setup.Left = 600; 
                setup.Bottom = 500; 
                setup.Width = 500; 
                setup.Height = 500;
                setup.pausePosition = [50 870 200 100];
                setup.axisPosition = [50 100 500 500];
                setup.graphPosition = [50 600 500 50];
                setup.successRatePosition = [600 500 300 100];
                setup.recentSuccessRatePosition = [600 250 300 100];
                setup.correctTargetPosition = [600 100 300 100];
        end
    case 'PCWIN64'
        % position for game window
        setup.xMin = -1600; 
        setup.yMin = 0; 
        setup.yMax = 900; 
        setup.xMax = 0;
        setup.xWidth = setup.xMax - setup.xMin; %1600
        setup.yWidth = setup.yMax - setup.yMin; %900
        % position for user window
        setup.Left = 2761; 
        setup.Bottom = 50; 
        setup.Width = 1000; 
        setup.Height = 1000;
        setup.pausePosition = [50 650 200 100];
        setup.axisPosition = [50 100 500 500];
        setup.graphPosition = [50 600 500 50];
        setup.successRatePosition = [600 500 300 100];
        setup.recentSuccessRatePosition = [600 250 300 100];
        setup.correctTargetPosition = [600 100 300 100];
        
        switch PCname
            case 'R2D2' 
                %% COORDINATE TRANSFORMATION 
                setup.Xmultiplier = 3.35;
                setup.Ymultiplier = 3.31;
                setup.fieldVolume = [0 480; 0 265; -35 35];   % the volume over the screen that in which markers are accepted as cursors [XMIN XMAX; YMIN YMAX; ZMIN ZMAX]
            case 'BB8'
                %% COORDINATE TRANSFORMATION 
                setup.Xmultiplier = 1.7;
                setup.Ymultiplier = 1.65;
                setup.fieldVolume = [0 940; 0 515; -35 35];% the volume over the screen that in which markers are accepted as cursors [XMIN XMAX; YMIN YMAX; ZMIN ZMAX]
            otherwise
                error('Cannot recognize PC name. Please start again.');
        end
        
end

setup.monitor = 0;                          % the monitor on which the game is played (this has to be 0 and cannot be changed)
% first field volumes/cursor size, are these necessary? - Shivalika 11/19
% setup.fieldVolume = [0 480; 0 265; -13 0];   % the volume over the screen that in which markers are accepted as cursors [XMIN XMAX; YMIN YMAX; ZMIN ZMAX]
% setup.cursorSize = 25;

% Original fieldVolumes (used for both rigs) - commented by Shivalika 11/19
% setup.fieldVolume = [0 480; 0 265; -35 35];   % the volume over the screen that in which markers are accepted as cursors [XMIN XMAX; YMIN YMAX; ZMIN ZMAX]
setup.cursorSize = 50;
% setup.fieldVolume = [0 480; 0 265; -13 0];   % the volume over the screen that in which markers are accepted as cursors [XMIN XMAX; YMIN YMAX; ZMIN ZMAX]
% setup.cursorSize = 25;
% setup.fieldVolume = [0 480; 0 265; -35 35];   % the volume over the screen that in which markers are accepted as cursors [XMIN XMAX; YMIN YMAX; ZMIN ZMAX]
% setup.cursorSize = 50;
 
%% RUNNING COUNTS
setup.successes = 0;
setup.fails = 0;
setup.holdInStartPenalties = 0;
setup.maxTimePenalties = 0;
setup.holdInTargetPenalties = 0;
setup.wrongChoicePenalties = 0;
setup.recentSuccessRate = 10;
 

 
%% MOTIVE - used in NatNetPollingInGame
setup.markers = 3;          %number of markers being used - default is 3 but more or less can be accounted for 
setup.cursorMarker_ID = 3;  %default ID of marker being used as a pseudo mouse - this number changes when a new marker enters the threshold

 
%% Data Structure
setup.save_frq = 1;        % save every # of trials
 
end

