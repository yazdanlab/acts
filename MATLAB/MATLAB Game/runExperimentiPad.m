% Shivalika Chavan, Kali Coubrough
% May 28, 2019
% Yazdan Lab
% runExperiment - main control function for starting experiments and
% calling different tasks
% set up GUI for stats box and main game screen

function data=runExperimentiPad

close all;
clearvars -except parameters;

clc
prompt = ['Name? (e.g. "TEST", "LITHIUM", etc) \n'];
monkeyName = upper(input(prompt, 's'));
macType = '';
PCname = '';
computerType = computer();
switch computerType
    case 'MACI64'
        usingMotive = 'no';
        natnetclient = [];
        clc
        
        %%% UPDATE: macType
        macType = 'MACBOOK';
        
        switch macType
            case 'IMAC'
                path_runPrgm = pwd;
                
                %%%UPDATE: FILEPATH
                mkdir (['/Users/yazdanlab/Documents/MATLAB/Data/' monkeyName], datestr(now, 29));
                
                %%%UPDATE: FILEPATH
                path_saveData = ['/Users/yazdanlab/Documents/MATLAB/Data/' monkeyName '/' datestr(now, 29)];
                
                %%%UPDATE: COMPUTER NAME
                computerName = "iMac";
            
            case 'MACBOOK'
                path_runPrgm = pwd;
                
                %%%UPDATE: FILEPATH
                mkdir (['/Users/nerdlab_dirtyside/Documents/MATLAB/Data from macbook air/' monkeyName], datestr(now, 29));
                
                %%%UPDATE: FILEPATH
                path_saveData = ['/Users/nerdlab_dirtyside/Documents/MATLAB/Data from macbook air/' monkeyName '/' datestr(now, 29)];
                
                %%%UPDATE: COMPUTER NAME
                computerName = "Macbook nerdlab_dirtyside";
            case 'PERSONAL LAPTOP'
                path_runPrgm = pwd;
                mkdir (['/Users/nerdlab_dirtyside/Documents/MATLAB/Data from macbook air/' monkeyName], datestr(now, 29));
                path_saveData = ['/Users/nerdlab_dirtyside/Documents/MATLAB/Data from macbook air/' monkeyName '/' datestr(now, 29)];
                computerName = "PERSONALLAPTOP";
        end
    case 'PCWIN64'
        path_runPrgm = 'C:\toolbox\Psychtoolbox\PsychBasic\MatlabWindowsFilesR2007a';
        
        prompt = 'Which PC? (e.g. "BB8", "R2D2") \n';
        PCname = upper(input(prompt, 's'));
        switch PCname
            case 'R2D2'
                mkdir (['C:\Users\user\Documents\Data\Data_Matlab Game\' monkeyName], datestr(now, 29));
                path_saveData = ['C:\Users\user\Documents\Data\Data_Matlab Game\' monkeyName '\' datestr(now, 29)];
                computerName = "PC R2D2";
                

            case 'BB8'
                mkdir (['C:\Users\labuser\Documents\Data\Data_Matlab Game\' monkeyName], datestr(now, 29));
                path_saveData = ['C:\Users\labuser\Documents\Data\Data_Matlab Game\' monkeyName '\' datestr(now, 29)];
                computerName = "PC BB8";
            otherwise
                error('Cannot recognize PC name. Please start again.');
        end
end

cd(path_runPrgm);
sca;

%skip default Psychtoolbox warning tasks
Screen('Preference','SkipSyncTests',1);
Screen('Preference','VisualDebugLevel',0);
Screen('Preference','SuppressAllWarnings', 1);
PsychDefaultSetup(2);

% get parameters that stay constant throughout experiment
SETUP_Parameters = Parameters_CONSTANT(computerType, macType, PCname);

% filename used for saving data in Data folder, formatted by date and time
% of when the experiment started
filename = SETUP_Parameters.filename;
numTrials = SETUP_Parameters.numTrials;

% setup screen coordinates
SCREEN_yMin = SETUP_Parameters.yMin;
SCREEN_yMax = SETUP_Parameters.yMax;
SCREEN_xMin = SETUP_Parameters.xMin;
SCREEN_xMax = SETUP_Parameters.xMax;
SCREEN_yMinHuman = SETUP_Parameters.yMinHuman;
SCREEN_yMaxHuman = SETUP_Parameters.yMaxHuman;
SCREEN_xMinHuman = SETUP_Parameters.xMinHuman;
SCREEN_xMaxHuman = SETUP_Parameters.xMaxHuman;
screenDimensions = [SCREEN_xMin SCREEN_yMin SCREEN_xMax SCREEN_yMax];
humanScreenDimensions = [SCREEN_xMinHuman SCREEN_yMinHuman SCREEN_xMaxHuman SCREEN_yMaxHuman];
monitorNumber = SETUP_Parameters.monitor; %the index of the monitor used
[window, ~]=Screen(monitorNumber, 'OpenWindow', [0 0 0], screenDimensions); %[0 0 0] means black
[humanWindow, ~]=Screen(monitorNumber, 'OpenWindow', [0 0 0], humanScreenDimensions);
Screen('Flip',window); % open window in rig, Flip means open or update the screen

%% Choose Task
clc;
prompt = ['Choose your task: ', ...
    '\n"1" for Instructed Delay Task', ...
    '\n"2" for Two Force Choice Task', ...
    '\n"3" for Training Task \n'];
taskChosen = input(prompt, 's');
successes = 0;
trialsCompleted = 0;

% initialize penalty counters, set categories for bar graph, make data
% structure (different for each task)
switch taskChosen
    case '1'
        taskName = 'INSTRUCTED DELAY';
        penalty_1 = 0;
        penalty_2 = 0;
        penalty_3 = 0;
        categories = categorical({'Successes', 'Penalty - No Hold In Start', 'Penalty - Max Time', 'Penalty - No Hold In Target'});
        data = struct('x', [], 'y', [], 'timeStamp', [], 'Angle', [], 'completion_Time', [], 'Successful', [], 'Time_Out', [], 'Parameters', [], 'Counts', []);
    case '2'
        taskName = 'TWO FORCE CHOICE';
        penalty_1 = 0;
        penalty_2 = 0;
        penalty_3 = 0;
        penalty_4 = 0;
        categories = categorical({'Successes', 'Penalty - No Hold In Start', 'Penalty - Max Time', 'Penalty - Wrong Circle', 'Penalty - No Hold In Target'});
        data = struct('x', [], 'y', [], 'timeStamp', [], 'completion_Time', [], 'Successful', [], 'Time_Out', [], 'Parameters', [], 'Left_Correct', [], 'targetChosen', [], 'Counts', []);
    case '3'
        taskName = 'TRAINING';
        penalty_1 = 0;
        penalty_2 = 0;
        categories = categorical({'Successes', 'Penalty - Max Time', 'Penalty - No Hold'});
        data = struct('x', [], 'y', [], 'timeStamp', [], 'completion_Time', [], 'Successful', [], 'Time_Out', [], 'Parameters', [], 'Counts', []);
        
    otherwise
        error('Task Not Found. Try Again.')
        sca;
        return;
end

%% Open GUI window for human side
clc;
position = [SETUP_Parameters.Left SETUP_Parameters.Bottom SETUP_Parameters.Width SETUP_Parameters.Height];
pausePosition = SETUP_Parameters.pausePosition;
t = 'User Control';
userWindow = figure('Position', position,'MenuBar','none','NumberTitle','off','Name',t);
pauseButton = uicontrol('Style','togglebutton','String','Pause', 'Fontsize', 24, 'Position', pausePosition, 'Callback', @pauseButtonPushed);

% Make generic stats window for GUI
ax = axes(userWindow);
set(gca,'FontSize',16)
ax.Units = 'pixels';
ax.Position = SETUP_Parameters.axisPosition;
chartTitle = ['Total Trials: ', num2str(trialsCompleted)];
graphTitle = uicontrol('style', 'text', 'Position', SETUP_Parameters.graphPosition, 'String', chartTitle, 'Fontsize', 15);

% Overall success rate for all trials (displayed on GUI)
successRate = successes/trialsCompleted*100;
str_sr = reshape({'Success Rate (%): ' num2str(successRate)}, 1, []);
statsBox_sr = uicontrol('style', 'text', 'Position', SETUP_Parameters.successRatePosition, 'String', str_sr, 'Fontsize', 15);

% Recent success rate for a set number of trials, can be edited in
% Parameters_CONSTANT.m
recentSuccessRate = successes/trialsCompleted*100;
str_rsr = reshape({'Recent Success Rate (%): ' num2str(recentSuccessRate)}, 1, []);
statsBox_rsr = uicontrol('style', 'text', 'Position', SETUP_Parameters.recentSuccessRatePosition, 'String', str_rsr, 'Fontsize', 15);
drawnow;

% Display whether left or right target is counted as correct for Two Force
% Choice Task
switch taskChosen
    case '2'
        correctTarget = "";
        str_ct = reshape({'Which target is correct: ' correctTarget}, 1, []);
        statsBox_ct = uicontrol('style', 'text', 'Position', SETUP_Parameters.correctTargetPosition, 'String', str_ct, 'Fontsize', 15);
        drawnow;
end

trial = 1;
%% Choose Parameter File
clc;
continueLoop = true;
while continueLoop
    errorCaught = false;
    clc
    disp('Choosing Parameter File');
    prompt = 'Enter name or default \n';
    parametersName = upper(input(prompt, 's'));
    
    %if nothing is typed in use default
    if(parametersName == "")
        parametersName = 'DEFAULT';
        disp('using default');
    end
    
    TASK_Parameters = Parameters_EDIT(parametersName, computerName, trial);
    try
        TASK_Parameters.reachDistance;
    catch
        clc
        disp('This parameter file does not exist! Try Again.')
        errorCaught = true;
    end
    if(~errorCaught)
        continueLoop = false;
    end
end

%% Run SATARS setup proceedure
clc
runSetup=true;
while runSetup
    switch computerType
        case 'MACI64'
            ui = input('Run setup proceedure? (y)es or (n)o: ', 's');
            switch ui
                case 'y'
                    TASK_Parameters = Parameters_EDIT(parametersName, computerName, 1);
                    for setupCount=1:5
                        switch taskChosen
                            case '1'
                                TASK_InstructedDelay(usingMotive, TASK_Parameters, SETUP_Parameters, natnetclient, window, computerType, macType, PCname, humanWindow);
                            case '2'
                                left_correct = 1;
                                TASK_TwoForceChoice(usingMotive, TASK_Parameters, SETUP_Parameters, natnetclient, window, left_correct, computerType, macType, PCname, humanWindow);
                            case '3'
                                TASK_TrainingCircle(usingMotive, TASK_Parameters, SETUP_Parameters, natnetclient, window, computerType, macType, PCname, humanWindow); 
                        end
                    end
                    [window, ~]=Screen(monitorNumber, 'OpenWindow', [0 0 0], screenDimensions); %[0 0 0] means black
                    Screen('Flip',window); % open window in rig, Flip means open or update the screen
                otherwise
                    runSetup=false;
            end
        otherwise
            runSetup=false;
    end
end

%% Final check before starting experiment
clc
Beeper;
disp('beep: verifying PsychToolbox sound...')
pause(2)
clc
prompt = ['\nNow would be a good time to bring in the subject. You have chosen: ', ...
    '\nAnimal: ' monkeyName, ...
    '\nTask: ' taskName, ...
    '\nParam File: ' parametersName, ...
    '\nNumber of Trials: ' num2str(numTrials), ...
    '\nIf these choices are correct and you wish to proceed, ', ...
    'press Enter/Return to immediately begin the game.', ...
    '\nIf these choices are incorrect, hit Ctrl+C / Command+C and start again.\n', ...
    '\nWhen the "Pause" button is pressed, the game will pause after the current trial is complete.\n'];
startExp = input(prompt, 's');


%% Start of experiment
disp('beep');
Beeper(TASK_Parameters.start_frq, TASK_Parameters.volume, TASK_Parameters.start_dur);
trial = 1;
data(trial).Parameters = TASK_Parameters;
while trial<=numTrials
    disp({'Current Trial', num2str(trial)});
    TASK_Parameters = Parameters_EDIT(parametersName, computerName, trial);
    switch taskChosen
        case '1' %TASK_InstructedDelay
            [data(trial).Angle, data(trial).x, data(trial).y, data(trial).timeStamp, data(trial).Successful, data(trial).completion_Time, data(trial).Time_Out, data(trial).Motive_Data, data(trial).Counts] = TASK_InstructedDelay(usingMotive, TASK_Parameters, SETUP_Parameters, natnetclient, window, computerType, macType, PCname, humanWindow);
        case '2' %TASK_TwoForceChoice
            
            if trial > TASK_Parameters.numPrevTrials
                probability = data(trial-1).Parameters.beep_probability;
                allTrialIndices = 1:(trial-1);
                allPrevOutcomes = [data(allTrialIndices).targetChosen]; % 1 for left target chosen, 0 for right target chosen, -1 if neither target is chosen
                targetChosenTrials = allTrialIndices(allPrevOutcomes == 0 | allPrevOutcomes ==1); % All outcomes where a target was chosen
                
                numChosenTrials = size(targetChosenTrials, 2);
                if numChosenTrials > TASK_Parameters.numPrevTrials
                    recentTrials = targetChosenTrials(end-TASK_Parameters.numPrevTrials+1:end); % keep only a set number of previous trials
                    %recentTrials = targetChosenTrials(end-min(size(targetChosenTrials, 2), TASK_Parameters.numPrevTrials)+1:end); % keep only a set number of previous trials THAT MONKEY CHOSE LEFT OR RIGHT
                    
                    recentOutcomes = [data(recentTrials).targetChosen]; % if 0 then monkey chose right target, 1 if left target was chosen
                    recentCorrect = [data(recentTrials).Left_Correct]; % if 0 then right target should have been chosen, 1 if left target
                    
                    leftChosen = sum((recentCorrect == 1 & recentOutcomes == 1) | (recentCorrect == 0 & recentOutcomes == 1));
                    rightChosen = sum((recentCorrect == 0 & recentOutcomes == 0) | (recentCorrect == 1 & recentOutcomes == 0));
                    probability = rightChosen./(leftChosen + rightChosen);
                end
            else
                probability = TASK_Parameters.beep_probability;
            end
            
            
            % probability compared against random number between 0 and 1 to
            % determine which circle will be considered correct by the
            % computer
            which_beep = rand;
            if which_beep < probability
                left_correct = 1;
            else
                left_correct = 0;
            end
            
            % Update which target is correct in the stats box
            if(left_correct == 1)
                correctTarget = 'left';
            else
                correctTarget = 'right';
            end
            str_ct = reshape({'Which target is correct: ' correctTarget}, 1, []);
            set(statsBox_ct, 'String', str_ct);
            drawnow;

            [data(trial).x, data(trial).y, data(trial).timeStamp, data(trial).Successful, data(trial).completion_Time, data(trial).Time_Out, data(trial).Motive_Data, data(trial).Counts, data(trial).Left_Correct, data(trial).targetChosen] = TASK_TwoForceChoice(usingMotive, TASK_Parameters, SETUP_Parameters, natnetclient, window, left_correct, computerType, macType, PCname, humanWindow);
            TASK_Parameters.beep_probability = probability;
            
        case '3' %TASK_TrainingCircle
            [data(trial).x, data(trial).y, data(trial).timeStamp, data(trial).Successful, data(trial).completion_Time, data(trial).Time_Out, data(trial).Motive_Data, data(trial).Counts] = TASK_TrainingCircle(usingMotive, TASK_Parameters, SETUP_Parameters, natnetclient, window, computerType, macType, PCname, humanWindow);
    end
    
    % Makes a black screen
    Screen('FillRect', window ,TASK_Parameters.screenColor ,screenDimensions );
    Screen('Flip',window);
    
    % update GUI
    successes = successes + data(trial).Counts(1);
    % counts array is full of 1 or 0, where a 1 indicates which type of
    % outcome a trial had
    % amount array is the totals for each outcome for all trials
    switch taskChosen
        case '1'
            penalty_1 = penalty_1 +  data(trial).Counts(2);
            penalty_2 = penalty_2 +  data(trial).Counts(3);
            penalty_3 = penalty_3 +  data(trial).Counts(4);
            amount = [successes penalty_1 penalty_2 penalty_3];
            
        case '2'
            penalty_1 = penalty_1 +  data(trial).Counts(2);
            penalty_2 = penalty_2 +  data(trial).Counts(3);
            penalty_3 = penalty_3 +  data(trial).Counts(4);
            penalty_4 = penalty_4 +  data(trial).Counts(5);
            amount = [successes penalty_1 penalty_2 penalty_3 penalty_4];
            
        case '3'
            penalty_1 = penalty_1 +  data(trial).Counts(2);
            penalty_2 = penalty_2 +  data(trial).Counts(3);
            amount = [successes penalty_1 penalty_2];
    end
    
    % update  game stats
    %%%%%%%
    trialsCompleted = sum(amount);
    successRate = successes/trialsCompleted*100;
    str_sr = reshape({'Success Rate (%): ' num2str(successRate)}, 1, []);
    set(statsBox_sr, 'String', str_sr);
    
    % update based on the adaptive probabilty
    switch taskChosen
        case '2'
            if(left_correct == 1)
                correctTarget = 'left';
            else
                correctTarget = 'right';
            end
            str_ct = reshape({'Which target is correct: ' correctTarget}, 1, []);
            set(statsBox_ct, 'String', str_ct);
    end
    
    recent_success = SETUP_Parameters.recentSuccessRate;
    if trial>recent_success
        recentSuccessRate = sum(data(trial-recent_success).Counts(1):data(trial).Counts(1))/recent_success*100;
        str_rsr = reshape({'Recent Success Rate (%): ' num2str(recentSuccessRate)}, 1, []);
        set(statsBox_rsr, 'String', str_rsr);
    else
        str_rsr = reshape({'Recent Success Rate (%): unactivated for first' num2str(recent_success) 'trials'}, 1, []);
        set(statsBox_rsr, 'String', str_rsr);
    end
    drawnow;
    chartTitle = ['Total Trials: ', num2str(trialsCompleted)];
    set(graphTitle, 'String', chartTitle);
    bar(categories, amount);
    drawnow;
    %%%%%%%%
    
    data(trial).Parameters = Parameters_EDIT(parametersName, computerName, trial); %update parameters file TODO: save TASK_parameters
    
    % keep black screen for set amount of time + any timeouts
    WaitSecs(data(trial).Time_Out + TASK_Parameters.TIMEOUT_Default);
    
    % save data at the end of each trials, previous file is overridden
    if(trial == numTrials || mod(trial, SETUP_Parameters.save_frq) == 0)
        cd(path_saveData)
        data_segment = data(1:trial);
        save(filename, 'data_segment');
        cd(path_runPrgm);
    end
    
    trial = trial +1;
    
    if trial > numTrials
        Beeper(300, 50, 0.3); pause(0.5);Beeper(300, 50, 0.3); pause(0.5);Beeper(300, 50, 0.3)
        s = ['If you would like to add more trials, enter the number (10, 20, etc)\n' ...
            'If you are done with the experiments, just press "Enter"\n'];
        extraTrials = uint8(floor(str2double(input(s, 's'))));
        if isinteger(extraTrials)
            addTrials = extraTrials;
        else
            addTrials = 0;
        end
        numTrials = numTrials + addTrials;
    end
end
%% Game complete
% keep black screen until user is ready to close screen
continueLoop = true;
while continueLoop
    disp('Done With Experiment. Now would be a good time to take subject out of the rig.');
    prompt = '\nEnter sca to close windows and end experiment \n';
    endStr = upper(input(prompt, 's'));
    switch usingMotive
        case 'no'
            try
                endStr == 'SCA';
                continueLoop = false;
                sca;
            catch
                disp('Command not found! Try Again');
            end
        case 'yes'
            if(endStr == 'SCA')
                continueLoop = false;
                natnetclient.stopRecord;
                cd(path_runPrgm);
                sca;
            end
    end
end

% add any additional notes
cd(path_saveData)
s = ['If you would like to add some notes about the experiment, enter them now.\n' ...
    '(Note that you can only enter in a single line, so you cant press enter to add multiple lines.\n' ...
    'If no notes, just press "Enter"\n'];
notes = input(s, 's');
filename = filename(1:end-4);
notes_filename = [filename '_Notes'];
notesFile = fopen(notes_filename, 'w+');
fprintf(notesFile, notes);
fclose(notesFile);
cd(path_runPrgm);
end


function pauseButtonPushed(hObject,~,~)
    % hObject    handle to togglebutton1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    button_state = get(hObject,'Value');
    if button_state == get(hObject,'Max')
        disp(['Game is paused. Now press the pause button in Matlab, and make changes to parameters. ', ...
            'When done, re-press the GUI pause button, and then click continue on Matlab.']);
        waitfor(hObject,'Value');
    elseif button_state == get(hObject,'Min')
        disp('Continuing...');
    end
end
      