% Shivalika Chavan, Kali Coubrough
% November 8, 2019
% Yazdan Lab
% getCursor - main function to collect 3D data from Motive
% OptiTrack software or mouse data
% 
% usingMotive (yes/no)
% natnetclient (interfaces with Optitrack data collection (see
% NatNetPollingInGame.m)
% SETUP_Parameters - parameter set 
% X, Y position vectors for cursor
% data_3d position matrix for 3D data
% ii loop variable

function [X, Y, data_3d] = getCursor(usingMotive, natnetclient, SETUP_Parameters, X, Y, data_3d, ii, computerType, macType, PCname)
    switch usingMotive
        case 'no'
            [x, y] = GetMouse(0, 1);
            Y(ii) = y;
            X(ii) = x;
        case 'yes'
            % Get 3D data here
            % have to collect as x, z, y coordinates because Motive
            % sees our z plane as its y plane so z and y need to be
            % switched when collecting 3D data from Motive. After this
            % switch, proceed with coordinate processing normally
            [data_3d(ii).names, data_3d(ii).x_3d, data_3d(ii).z_3d, data_3d(ii).y_3d, ~] = NatNetPollingInGame(natnetclient, 0, computerType, macType, PCname);

            % check to see if the marker is in the designated volume in
            % front of the screen
            SETUP_Parameters.cursorMarker_ID = inVolume(data_3d(ii).x_3d, data_3d(ii).y_3d, data_3d(ii).z_3d, SETUP_Parameters.fieldVolume);


            if ~(SETUP_Parameters.cursorMarker_ID == -1)
                x = data_3d(ii).x_3d(SETUP_Parameters.cursorMarker_ID);
                y = data_3d(ii).y_3d(SETUP_Parameters.cursorMarker_ID);

                %fit the coordinates to the screen
                x = SETUP_Parameters.Xmultiplier*(x + SETUP_Parameters.offset);
                y = SETUP_Parameters.Ymultiplier*(y + SETUP_Parameters.offset);

            else
                %none of the cursors are in the volume
                x = -1;
                y = -1;
            end
            Y(ii) = y;
            X(ii) = x;
    end
end