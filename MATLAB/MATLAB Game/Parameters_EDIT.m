function parameters = Parameters_EDIT(parameterSet, computerName, trial)
% frequencies - frq
% durations - dur
% enter names in all upper case
try
    parameters.nameFile = parameterSet;
catch
    disp('No Parameter file specified. Using DEFAULT..');
    parameterSet = 'DEFAULT';
end

parameters.computerUsed = computerName;
parameters.date = datetime;

switch parameterSet
    case 'DEFAULT'
        %% TASK
        parameters.reachDistance = 300;         % distance between the middle of start and end targets
        parameters.waitTime_BeforeGoTone = 2;   % maximum waiting interval after first target before go tone
        parameters.maxTime = 100000000000;      % maximum time each trial can go for before timing out
        parameters.startCircleSize = 100;       % start circle (green) radius in pixels
        parameters.time_moveIntoStartTarget = 30;
        parameters.TimeToShowCircle = 1;        % when to show circle
        parameters.holdTouch = 'yes';           % drives whether a hold is required for successful trial
        parameters.holdInEnd = 0;               % time monkey must hold in end target for successful trial
        parameters.holdInStart = 0;             % time monkey must hold in start target before go tone sounds
        if(parameters.holdInEnd==0 && parameters.holdInStart==0)
            parameters.holdTouch = 'no'; % if both hold times are 0, no hold required (only takes into account cursor position)                  
        end
        % IMPORTANT: when a hold time (holdTouch = 'yes') is required, the 
        % subject can only have one point of contact with the iPad. 
        % If the subject places multiple fingers in the target, the hold 
        % will not work. ('button' array must contain a '1' for successful 
        % hold (single touch), with multitouch the array will be all zeros)
        
        %% TRAINING TASK
        parameters.CircleSize = 100;
        parameters.TimeToShowCircle = 1; %wait this many seconds to show big circle
        parameters.hiddenRadius = 100;          % area that is counted as correct but is different than the size of the circle the user sees
        if(parameters.hiddenRadius > parameters.CircleSize)
            parameters.hiddenRadius = parameters.CircleSize;
            disp('Hidden radius cannot be larger than circle size. Hidden radius has been automatically set to equal the circle size.')
        end
        
        %% REWARD
        parameters.successfulTime = 500;
        parameters.earlyPenaltyReward = 0;
        parameters.longPenaltyReward = 0;
        
        %% TIMEOUTS
        parameters.TIMEOUT_Default = 0.5;              % default delay between trials
        parameters.TIMEOUT_Hold = 3;  % timeout length for above penalty
        parameters.TIMEOUT_MaxTime = 2;   % timeout length for above penalty
        parameters.TIMEOUT_WrongChoice = 0; % wrong choice
        %% COLOR
        parameters.startTargetColor = [0 255 0];    %default start target color is green
        parameters.endTargetColor = [255 0 0];      % default end target color is red
        parameters.screenColor = [0 0 0];           %default black screen color
        
        %% SOUND
        parameters.volume = 10;     % volume for all tones
        parameters.start_frq = 500; % before first trial
        parameters.start_dur = 0.5000;
        parameters.go_frq = 700;    % go tone in each trial
        parameters.go_dur = 0.5000;
        parameters.right_frq = 400;
        parameters.left_frq = 800;
        
        %% PROBABILITY
        parameters.beep_probability = 0.5;
        parameters.numPrevTrials = 5;
               
        %% BUFFER
        parameters.bufferTime = 0;
      
    case 'TRAINING'
        %% TASK
        parameters.reachDistance = 600;         %distance between the middle of start and end targets
        parameters.waitTime_BeforeGoTone = 2;   % maximum waiting interval after first target before go tone
        parameters.maxTime = 100000;               % maximum time each trial can go for before timing out
        parameters.holdTouch = 'yes';           % drives whether a hold is required for successful trial
        parameters.holdInEnd = 0;               % time monkey must hold in end target for successful trial
        parameters.holdInStart = 0;             % time monkey must hold in start target before go tone sounds
        if(parameters.holdInEnd==0 && parameters.holdInStart==0)
            parameters.holdTouch = 'no';        % if both hold times are 0, no hold required (only takes into account cursor position)
        end
        % IMPORTANT: when a hold time (holdTouch = 'yes') is required, the 
        % subject can only have one point of contact with the iPad. 
        % If the subject places multiple fingers in the target, the hold 
        % will not work. ('button' array must contain a '1' for successful 
        % hold (single touch), with multitouch the array will be all zeros)
        
        parameters.delayTrial = 0;              % default delay between trials
        parameters.time_moveIntoStartTarget = 30;
        parameters.TimeToShowCircle = 1;        % when to show circle
        parameters.CircleSize = 200;      % this gets bigger and smaller - max size is 420
        parameters.hiddenRadius = 200;          % area that is counted as correct but is different than the size of the circle the user sees
        if(parameters.hiddenRadius > parameters.CircleSize)
            parameters.hiddenRadius = parameters.CircleSize;
            disp('Hidden radius cannot be larger than circle size. Hidden radius has been automatically set to equal the circle size.')
        end
            
        
        %% REWARD
        parameters.successfulTime = 500;
        parameters.earlyPenaltyReward = 100;
        parameters.longPenaltyReward = 200;
        
        %% TIMEOUTS
        parameters.TIMEOUT_Default = 1;              % time added to delay if in time out
        parameters.TIMEOUT_Hold = 0;  % timeout length for above penalty
        parameters.TIMEOUT_MaxTime = 5;   % timeout length for above penalty
        
        %% COLOR
        parameters.startTargetColor = [0 255 0];    %default start target color is green
        parameters.endTargetColor = [0 255 0];      % default end target color is red
        parameters.screenColor = [0 0 0];           %default black screen color
        %% SOUND
        parameters.volume = 10;     % volume for all tones
        parameters.start_frq = 500; % before first trial
        parameters.start_dur = 0.5000;
        parameters.go_frq = 700;    % go tone in each trial
        parameters.go_dur = 0.5000; 
        
        %% BUFFER
        parameters.bufferTime = 0;
        
end
end

