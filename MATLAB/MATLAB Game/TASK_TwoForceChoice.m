% Shivalika Chavan, Kali Coubrough
% May 6, 2019
% Yazdan Lab
% Two Force Choice Task - Target circle appears after a tone, then two
% identical circles appear on either side of the inital circle. User must
% then touch either left or right circle depending on tone frequency or
% other stimulus. Probability is adjusted based off of past choices.

function [X, Y, timeStamp, successful, completionTime, extraTimeOut, data_3d, counts, left_correct, targetChosen] = TASK_TwoForceChoice(usingMotive, TASK, SETUP, natnetclient, window, left_correct, computerType, macType, PCname, humanWindow)

% build initial circle
center_X = 0.5*SETUP.xWidth;
center_Y = 0.5*SETUP.yWidth;
radius = TASK.startCircleSize;
x1 = center_X-radius; y1 = center_Y-radius;
x2 = center_X+radius; y2 = center_Y+radius;

targetChosen = -1;

%startTone
addInXDir = TASK.reachDistance;

Y = zeros(1, 1300);
X = zeros(1, 1300);
timeStamp = nan(1, 1300);
successful = true;

inLeft = false; inRight = false;
inTarget = false; 
showTarget = false; 

showStartCircle = false;
doneWithTask = false;
beeped = false;

% determines if hold will be taken into account
holdTouch = TASK.holdTouch;

startTimer_Start = -1;
startTimer_Target = -1;
time_showTarget = -1;
bufferStartTime = -1;

waitSecs_BeforeGoTone = rand(1)*TASK.waitTime_BeforeGoTone;

%first get coordinates, then flip screen for first iteration
%in each iteration after check where the cursor is and continue if
%necessary
ii = 0;
startTask = GetSecs;
touchedStart = false;
touchedTarget = false;
buffer = false;

data_3d = struct('names', [], 'x_3d', [], 'y_3d', [], 'z_3d', []);
extraTimeOut = 0;
while (~doneWithTask || buffer)
    ii = ii + 1;
    timeStamp(ii) = GetSecs-startTask;
    
    if buffer
        Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
        Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
        if timeStamp(ii)-bufferStartTime > TASK.bufferTime
            break
        end
    end
    
    [X, Y, data_3d] = getCursor(usingMotive, natnetclient, SETUP, X, Y, data_3d, ii, computerType, macType, PCname);
    x = X(ii);
    switch computerType
        case'MACI64'
            x = x - SETUP.xMin;
    end
    y = Y(ii);
    
    switch computerType
        case 'PCWIN64'
            Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);
        case'MACI64'
            Screen('DrawDots', window, [x y], SETUP.cursorSize, [255 255 255], [], 2);
            Screen('DrawDots', humanWindow, [x y]./2, (SETUP.cursorSize)./2, [255 255 255], [], 2);
    end
    Screen('Flip',window);
    Screen('Flip',humanWindow);
    
    if buffer
        continue;
    end
    
    % after one second after the start of the task, show the initial circle
    if(GetSecs-startTask>1)
        Screen('FillOval',window, TASK.startTargetColor,[x1 y1 x2 y2]);
        Screen('FillOval',humanWindow, TASK.startTargetColor,[x1 y1 x2 y2]./2);
        showStartCircle = true;
    end
    
    % check if cursor is in first circle 
    if(pdist([x y; mean([x1 x2]) mean([y1 y2])],'euclidean') <= radius && showStartCircle) 
        inStart = true;
        touchedStart = true;
    else
        inStart = false;
    end
    
    % the stopwatches for the start and target timers should only start
    % when both the cursor is within the desired circle and the respective
    % timer has not started
    % This should only happen once per trial (any other outcomes are a penalty)
    switch computerType
        case 'PCWIN64'
            
            if (inStart && startTimer_Start == -1)
                startTimer_Start = GetSecs;
            end
            
            if(inTarget && startTimer_Target == -1)
                startTimer_Target = GetSecs;
            end
            
        case 'MACI64'
            
            if (inStart && startTimer_Start == -1)
                switch holdTouch
                    case 'yes'
                        [~,~,button] = GetMouse(); % Check to see if the mou3se is held down for timer to start
                        if(button(1) == 1)
                            startTimer_Start = GetSecs;
                        end
                    case 'no'
                        startTimer_Start = GetSecs;
                end
            end
            
            if(inTarget && startTimer_Target == -1)
                switch holdTouch
                    case 'yes'
                        [~,~,button] = GetMouse(); % Check to see if the mouse is held down for timer to start
                        if(button(1) == 1)
                            startTimer_Target = GetSecs;
                        end
                    case 'no'
                        startTimer_Target = GetSecs;
                end
            end
            
    end
    
    % if the start timer has been started (equal to anything other than -1)
    if(~(startTimer_Start == -1))
        % if the initial circle timer shows a time greater than what is 
        % needed to show the target AND the target has not been shown yet, 
        % then show the target - 
        % This should only happen once per trial (any other outcomes are a penalty)
        if(GetSecs-startTimer_Start > TASK.holdInStart && ~showTarget)
            showTarget = true;
        end
    end
    
    % reference if statement above - same logic but with the target circle
    if(~(startTimer_Target == -1))
        if(GetSecs-startTimer_Target > TASK.holdInEnd)
            successful = true;
            doneWithTask = true;
        end
    end
    
    % if the target should be shown (either true or false)
    % two end targets show (right now both are counted as correct)
    % redefine y values in beginning of this file
    if showTarget
        Screen('FillOval',window,TASK.endTargetColor,[x1+addInXDir y1 x2+addInXDir y2])
        Screen('FillOval',window,TASK.endTargetColor,[x1-addInXDir y1 x2-addInXDir y2])
        Screen('FillOval',humanWindow,TASK.endTargetColor,[x1+addInXDir y1 x2+addInXDir y2]./2)
        Screen('FillOval',humanWindow,TASK.endTargetColor,[x1-addInXDir y1 x2-addInXDir y2]./2)
        
        %There is another timer that starts when the target circle appears.
        % This starts that time if it has not started yet.
        if(time_showTarget == -1)
            time_showTarget = GetSecs;
        end
        
        % if the tone has not sounded and the timer has surpassed what is
        % necessary for the beep, then beep - successful trial so far
        if(~beeped && GetSecs-time_showTarget>waitSecs_BeforeGoTone)
            if left_correct
                switch computerType
                    case 'PCWIN64'
                        Beeper(TASK.left_frq, TASK.volume, TASK.go_dur);
                    case 'MACI64'
                        PlayTone(1);
                end
            else
                switch computerType
                    case 'PCWIN64'
                        Beeper(TASK.left_frq, TASK.volume, TASK.go_dur);
                    case 'MACI64'
                        PlayTone(0);
                end
            end
            beeped = true;
        end
    end  
    
    % the target circles are displayed
    if beeped
        % check which circle was chosen
        if pdist([x y; mean([x1 x2])-addInXDir mean([y1 y2])],'euclidean') <= radius
            inLeft = true;
            inRight = false;
            targetChosen = 1;
        elseif pdist([x y; mean([x1 x2])+addInXDir mean([y1 y2])],'euclidean') <= radius
            inLeft = false;
            inRight = true;
            targetChosen = 0;
        else 
            inTarget = false;
            inLeft = false;
            inRight = false;
        end
        
        % check if correct circle is chosen and if cursor is still in the
        % circle
        if(inLeft && left_correct) || (inRight && ~left_correct)
            inTarget = true;
            touchedTarget = true;
            touchedStart = false;
        elseif (inLeft && ~left_correct) || (inRight && left_correct)
            extraTimeOut = TASK.TIMEOUT_WrongChoice;
            inTarget = false;
            successful = false;
            doneWithTask = true;
            SETUP.wrongChoicePenalties = SETUP.wrongChoicePenalties+1;
            SETUP.fails = SETUP.fails+1;
        else
            inTarget = false;
        end
    end
    
    %penalty for not holding in a cirle
    if (~beeped && ~inStart && touchedStart) || (beeped && ~inTarget && touchedTarget)
        doneWithTask=true;
        extraTimeOut = TASK.TIMEOUT_Hold;
        successful = false;
        if(touchedStart)
            SETUP.holdInStartPenalties = SETUP.holdInStartPenalties+1;
        elseif(touchedTarget)
            SETUP.holdInTargetPenalties = SETUP.holdInTargetPenalties+1;
        end
        SETUP.fails = SETUP.fails+1;
    end
    
    
    %penalty for taking too long
    if(GetSecs-startTask > TASK.maxTime)
        extraTimeOut = TASK.TIMEOUT_MaxTime;
        successful = false;
        doneWithTask = true;
        SETUP.maxTimePenalties = SETUP.maxTimePenalties+1;
        SETUP.fails = SETUP.fails+1;
    end
    
    % The success tone plays before the circle disappears
    if(successful && doneWithTask)
        switch computerType
            case 'MACI64'
                RunFeederAndPlayTone;
            case 'PCWIN64'
                % insert reward command or do nothing
        end
        filename = 'success_tone.wav';
        [n, Fs] = audioread(filename);
        audiowrite(filename,n,Fs);
        sound(n(26000:150000),Fs);
    end
    
    % finish with a black screen when done with task
    if(doneWithTask)
        buffer = true;
        Screen('FillRect', window ,TASK.screenColor ,[SETUP.xMin SETUP.yMin SETUP.xMax SETUP.yMax] );
        Screen('FillRect', humanWindow ,TASK.screenColor ,[SETUP.xMinHuman SETUP.yMinHuman SETUP.xMaxHuman SETUP.yMaxHuman]);
    end
    
    if buffer
        bufferStartTime = timeStamp(ii);
    end
    
end

completionTime = GetSecs-startTask;

% Updates the number of successes after each trial
if(successful)
    SETUP.successes = SETUP.successes+1;
end
counts = [SETUP.successes, SETUP.holdInStartPenalties, SETUP.maxTimePenalties, SETUP.wrongChoicePenalties, SETUP.holdInTargetPenalties];
end