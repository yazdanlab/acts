function [addInXDir, addInYDir, angle] = buildTargetCircle(x1, x2, y1, y2, TASK_Parameters, SETUP_Parameters)
    setTarget = false;
    while ~setTarget
        angle = 180*rand(1);
        radian = (angle*pi)/180;
        addInYDir = TASK_Parameters.reachDistance*sin(radian);
        addInXDir = TASK_Parameters.reachDistance*cos(radian);
        % check that the coordinates selected are within the bounds of the
        % screen
        if(x1+addInXDir>0 && y1-addInYDir>0 && x2+addInXDir<SETUP_Parameters.xWidth && y2-addInYDir<SETUP_Parameters.yWidth)
            setTarget = true;
        else
            setTarget = false;
        end
    end
end