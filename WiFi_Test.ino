// WiFi_Test.ino
// Devon J Griggs
// 8/14/19
//
// Modified from https://tttapa.github.io/ESP8266/Chap07%20-%20Wi-Fi%20Connections.html
// Modified from https://tttapa.github.io/ESP8266/Chap10%20-%20Simple%20Web%20Server.html


// Libraries
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h> 
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>

// Create a webserver object that listens for HTTP request on port 80
ESP8266WebServer server(80);

// function prototypes
void handleRoot();              
void handleLED();        
void handleMotor();       
void handleHighTone();
void handleLowTone(); 
void handleMotorAndTone();    
void handleNotFound();
void flashLED();

// constants
const char* ssid     = "NETWORK_NAME";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* password = "NETWORK_PASSWORD";     // The password of the Wi-Fi network

// variables
int state = 0;  // state for serial reading
int i = 0;      // iterater


void setup() {
  // Initialize pins
  pinMode(LED_BUILTIN , OUTPUT);
  pinMode(0           , OUTPUT);

  // Set pins high
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(0          , HIGH);
  
  // Start the Serial communication to send messages to the computer
  Serial.begin(115200);         
  delay(10);
  Serial.println('\n');

  // Connect to the network
  WiFi.begin(ssid, password);             
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");

  // Wait for the Wi-Fi to connect
  i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }
  
  // Celebrate connection
  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");

  // Send the IP address of the ESP8266 to the computer
  Serial.println(WiFi.localIP());         

  // Set up the server
  server.on("/"             , HTTP_GET, handleRoot          );
  server.on("/LED"          , HTTP_GET, handleLED           );
  server.on("/Motor"        , HTTP_GET, handleMotor         );
  server.on("/HighTone"     , HTTP_GET, handleHighTone      );
  server.on("/LowTone"      , HTTP_GET, handleLowTone       );
  server.on("/MotorAndTone" , HTTP_GET, handleMotorAndTone  );
  server.onNotFound(handleNotFound);        
  server.begin();                           
  Serial.println("HTTP server started");

  // Initialize pins
  pinMode(LED_BUILTIN , OUTPUT);
  pinMode(0           , OUTPUT);

  // Set pins high
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(0          , HIGH);

  // Flash LEDs to indicate that the setup is complete and that the loop is starting
  flashLED();
}


void loop() { 
  // Listen for HTTP requests from clients
  server.handleClient();                    

  // Look for serial input and read if any
  if(Serial.available() > 0){
    state = Serial.read();
  }else{
    state = 0;
  }

  // Serial write IP address
  delay(10);
  Serial.println(WiFi.localIP()); 
}


// Handle functions
void handleRoot() {
  // do nothing
}

void handleMotor() {
  // run motor
  digitalWrite(0, LOW);
  delay(100);
  digitalWrite(0, HIGH);
}

void handleLED() {                          
  // blink LED
  digitalWrite(LED_BUILTIN, LOW);
  delay(50);
  digitalWrite(LED_BUILTIN, HIGH);
}

void handleHighTone() {
  // play tone (and turn on the LED)
  i=0;
  analogWriteFreq(3000);
//    analogWrite(LED_BUILTIN,511);
  analogWrite(LED_BUILTIN,511);
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
}

void handleLowTone() {
  // play tone (and turn on the LED)
  i=0;
  analogWriteFreq(2000);
//    analogWrite(LED_BUILTIN,511);
  analogWrite(LED_BUILTIN,511);
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
}


void handleMotorAndTone(){
  // run motor
  digitalWrite(0, LOW);
  delay(100);
  digitalWrite(0, HIGH);
  
  // play tone (and turn on the LED)
  i=0;
  while(i<5){
    analogWriteFreq(3000);
//    analogWrite(LED_BUILTIN,511);
    analogWrite(LED_BUILTIN,511);
    delay(200);
    analogWriteFreq(2000);
//    analogWrite(LED_BUILTIN,511);
    analogWrite(LED_BUILTIN,511);
    delay(200);
    i++;
  }
  digitalWrite(LED_BUILTIN, HIGH);
}

void handleNotFound(){
  // In the event where a command is sent that is not understood, blink the LED slowly twice.
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
}


// Helper functions
void flashLED(){
  // Flash LED quickly several times
  i = 0;
  while (i<50){
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);     // Turn the LED on
    delay(50);
    digitalWrite(LED_BUILTIN, HIGH);    // Turn the LED off
    i++;
  }
}
