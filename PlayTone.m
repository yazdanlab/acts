function PlayTone(tone)
%PlayTone Plays a tone on the WiFi chip
%   Gives an output to play a tone on the wifi chip for in-cage traininging. Must be run after the
%   ESP8266 chip is plugged in and set up. This code pings what is usually
%   the IP address of the chip to cause the motor to start spinning. If the
%   code is not working, check to see that the computer is running on
%   bioe-i3xx-local Wi-Fi.
%
%   Devon Griggs
%   7/23/2019
%   Initial release
%
%   Devon Griggs
%   12/9/2019
%   Included multi-tone functionality


switch tone
    case 0
        system(['curl --silent http://',getIP,'/HighTone > /dev/null']);
    case 1
        system(['curl --silent http://',getIP,'/LowTone > /dev/null']);
    otherwise
        disp('Tone input not recognized.')
end
end

