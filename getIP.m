function [IP_str] = getIP
%getIP Gets a pre-programmed IP address for running the automatic training
%system
%   If the code is not working, the hard-coded IP address may be incorrect.
%   To investigate, enter
%   "[x,y]=system('arp -a')" in MATLAB command line and look to see what IP
%   addresses are listed and update the code accordingly. Note that IP
%   address 192.168.1.1 is almost certainly the router and therefore is not
%   the IP address of interest.
%
%   Devon Griggs
%   12/9/2019
%   Initial release
IP_str='192.168.1.101';
end

